<snippet>
  <content><![CDATA[
# 1:Project Name: Voice Recognition

Voice recognition project helps in converting the voice into text and manipulating into performable actions
    -Saving the converted text in a file(Prop file) from which the test suites can be defined on the go with Voice commands
    -The execution occurs for 2 platform - Mobile and Web 
    -Certain other features are also incorporated of interacting with voice assitance(Bing) where in we can perform actions based on it


## Installation
System should be installed with the mentioned softwares:
    -NPM
    -Appium
    _android studio
    -python3
    -homebrew
    -pip libraries
        -pip install pyaudio
        -pip install --upgrade pyaudio
        -pip install wheel
        -pip install google-api-python-client
        -sudo apt-get install flac
        -pip install monotonic
        -pip install SpeechRecognition
        -pip install properties
        -pip install Jproperties
    -selenium jar
    -chromedriver

    


## Credits

#3 Musketeers:
    #Thiruneelakandan Rajagopal
    #Rohini Srinivasan
    #Arunmozhi Varman
    #Santhosh

]]></content>
  <tabTrigger>readme</tabTrigger>
</snippet>