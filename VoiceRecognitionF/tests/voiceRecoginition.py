#Author: Thiruneelakandan Rajagopal;Rohini Srinivasan
import pyttsx3
import speech_recognition as sr
import time
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from appium import webdriver as webdriver1
from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time
import sys
from wrapper.listenSpeak import *
from wrapper.propertyHandler import *


filename="config.properties"
#Function to choose the driver option for chrome
def DriverCapsWeb():
    SpeakText("Please choose the driver to start")
    driverCaps = ListenText()
    if ("chrome" or "browser" or "open" or "driver") in driverCaps:
        driver = webdriver.Chrome()
        return driver
    elif ("fire" or "fox") in driverCaps:
        print("not available")
        SpeakText("Sorry!. Since it is a Beta version we do not have that option")
        sys.exit(0)
    else:
        print("not available")
        SpeakText("Sorry!. Since it is a Beta version we do not have that option")
        sys.exit(0)
      
       
 
SpeakText("Hey I am your assistance Optimus.! What do you want to do?. Since we are in Beta test!.Stick to the Menu Options !. Do you want to open? Web? Or Mobile ?") 

local_browse = ListenText()       
if ("mobile" or "update" or "capabilities") in local_browse:
    file1 = open(filename,"w")
    file1.write("#Desired Capabilities\n")
    SpeakText("Select your execution suite!. Chrome App!. Citi App!") 
    ExecutionSuite = ListenText()

    if ("chrome" or "browser" or "mobile") in ExecutionSuite:
        SpeakText("Speak your Desired Capability for Platform Name for Chrome") 
        desiredCapsPN = ListenText()
        LPlatformName = "platformName = Android\n"
        if ("android" or "aos") in desiredCapsPN:
            append_property(filename, LPlatformName)
        else:
            SpeakText("Not a valid Platform name, Adding default for the suite")
            append_property(filename, LPlatformName)
    #AutomationName
        SpeakText("Speak your Desired Capabilty for Automation Name") 
        desiredCapsAN = ListenText()
        LautomationName = "automationName = UiAutomator2\n"
        Ldevice="udid = emulator-5554\n"
        if ("ui" or "automator") in desiredCapsAN:
            append_property(filename, LautomationName)
            append_property(filename, Ldevice)
        else:
            SpeakText("Not a valid Automation name, Adding default for the suite")
            append_property(filename, LautomationName)
            append_property(filename, Ldevice)
    #appPackage
        SpeakText("Speak your Desired Capability for App package") 
        desiredCapsAP = ListenText()
        LAppPackage = "appPackage = com.android.chrome\n"
        LAppAcitivity = "appActivity = com.google.android.apps.chrome.Main\n"
        if ("chrome" or "browser") in desiredCapsAP:
            append_property(filename, LAppPackage)
            append_property(filename, LAppAcitivity)
        else:
            SpeakText("Not a valid Package name, Adding default for the suite")
            append_property(filename, LAppPackage)
            append_property(filename, LAppAcitivity)
        
        desired_caps=get_property_dict()

        driver = webdriver1.Remote("http://127.0.0.1:4723/wd/hub", desired_caps)
    
        time.sleep(3)
        element = driver.find_element_by_id("com.android.chrome:id/terms_accept")
        element.click()
        time.sleep(3)
        element = driver.find_element_by_id("com.android.chrome:id/negative_button")
        element.click()
        time.sleep(5)
        element = driver.find_element_by_id("com.android.chrome:id/search_box_text")
        SpeakText("What do you want to search")
        time.sleep(10)
        element.send_keys(ListenText())
        time.sleep(5)
        element = driver.find_element(By.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.ListView/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView')
        element.click()
        time.sleep(10)
        driver.quit()
        
    elif("citi" or "banking" or "city" or "app"):
        SpeakText("Speak your Desired Capabilty for Platform Name for Citi app") 
        desiredCapsPN = ListenText()
        LPlatformName = "platformName = Android\n"
        if ("android" or "aos") in desiredCapsPN:
            append_property(filename, LPlatformName)
        else:
            SpeakText("Not a valid Platform name, Adding default for the suite")
            append_property(filename, LPlatformName)
    #AutomationName
        SpeakText("Speak your Desired Capabilty for Automation Name")
        desiredCapsAN = ListenText()
        LautomationName = "automationName = UiAutomator2\n"
        Ldevice="udid = emulator-5554\n"
        if ("ui" or "automator" or "2") in desiredCapsAN:
            append_property(filename, LautomationName)
            append_property(filename, Ldevice)
        else:
            SpeakText("Not a valid Automation name, Adding default for the suite")
            append_property(filename, LautomationName)
            append_property(filename, Ldevice)
    #appPackage
        SpeakText("Speak your Desired Capabilty for App package") 
        desiredCapsAP = ListenText()
        LAppPackage = "appPackage = com.citi.mobile.th.sit\n"
        LAppAcitivity="appActivity = com.citi.mobile.pt3.starter.authentication.view.activity.LoginActivity\n"
        if ("citi" or "city" or "com" or "sit" or "set") in desiredCapsAP:
            append_property(filename, LAppPackage)
            append_property(filename, LAppAcitivity)
        else:
            SpeakText("Not a valid App package, Adding default for the suite")
            append_property(filename, LAppPackage)
            append_property(filename, LAppAcitivity)
        
        desired_caps_mobile=get_property_dict()
        print(desired_caps_mobile)
        driver = webdriver1.Remote("http://127.0.0.1:4723/wd/hub", desired_caps_mobile)
        time.sleep(3)
        element = driver.find_element_by_id("com.citi.mobile.th.sit:id/continue_button")
        element.click()
        time.sleep(10)
        element = driver.find_element_by_id("com.android.permissioncontroller:id/permission_allow_button")
        element.click()
        time.sleep(10)
        element = driver.find_element_by_id("com.android.permissioncontroller:id/permission_allow_foreground_only_button")
        element.click()
        time.sleep(10)
        SpeakText("App crashed ! reopening again !")
        driver.activate_app("com.citi.mobile.th.sit")
        time.sleep(20)
        
        element = driver.find_element_by_id("com.citi.mobile.th.sit:id/continue_button")
        element.click()
        time.sleep(15)
        element = driver.find_element(By.XPATH,"//*[@text='English']")
        element.click()
        time.sleep(5)
        element.click()
        time.sleep(15)
        element = driver.find_element(By.XPATH,"//android.widget.RelativeLayout[contains(@resource-id,'id/accept_button')]")
        element.click()
        time.sleep(15)
        SpeakText("we have landed on Citi Login Page!. We have successfully completed the On boarding")
        element = driver.find_element_by_id("com.citi.mobile.th.sit:id/login_button")
        element.click()
        time.sleep(5)
    else:
        SpeakText("Unknown suite ! try again!")

#Web search via voice        
elif ("web" or "browser" or "driver") in local_browse:
    driver=DriverCapsWeb()
    driver.get("https://www.bing.com")
    element = driver.find_element_by_id("sb_form_q")
    SpeakText("What do you want to search") 
    element.send_keys(ListenText())
    time.sleep(2)
    element.send_keys(Keys.RETURN)
    time.sleep(5)
    driver.close()

#Bing Voice Interaction
elif ("voice" or "bing" or "search" or "interaction") in local_browse:
    opt = Options()
    opt.add_argument('--disable-blink-features=AutomationControlled')
    opt.add_argument('--start-maximized')
    opt.add_experimental_option("prefs", {
 
    "profile.default_content_setting_values.media_stream_mic": 1,
    "profile.default_content_setting_values.media_stream_camera": 1,
    "profile.default_content_setting_values.geolocation": 0,
    "profile.default_content_setting_values.notifications": 1
})
    driver = webdriver.Chrome(options=opt)
    driver.get("https://www.bing.com")
    time.sleep(4)
    element = driver.find_element_by_xpath("//*[@class='mic_cont icon']")
    element.click()
    time.sleep(20)
    SpeakText("do you want to continue the conversation! Or start a new one") 
    Conversation=ListenText()
    if ("yes" or "continue" or "go ahead" or "new") in Conversation:
        element = driver.find_element_by_xpath("//*[@class='b_icon tooltip']")
        element.click()
        time.sleep(25)
    elif ("no" or "not interested" or "quit") in Conversation:
        driver.quit
    print("Time over, thank you")
    driver.close()

 
