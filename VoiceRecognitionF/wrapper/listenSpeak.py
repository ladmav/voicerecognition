#Author: Thiruneelakandan Rajagopal;Rohini Srinivasan
#Method Usage: Common methods for converting text to speech,Listen to text

import pyttsx3
import speech_recognition as sr
import winsound
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# Initialize the recognizer
r = sr.Recognizer() 
r.energy_threshold = 300
frequency = 500  # Set Frequency To 2500 Hertz
duration = 500 

# Function to convert text to speech
def SpeakText(command):
      
    # Initialize the engine
    engine = pyttsx3.init()
    engine.say(command) 
    engine.runAndWait()
      
# Function to listen user speak
def ListenText():
    while(1):   
    # Exception handling to handle
    # exceptions at the runtime
        try:
        # use the microphone as source for input.
            with sr.Microphone() as source2:
            # wait for a second to let the recognizer
            # adjust the energy threshold based on
            # the surrounding noise level 
                r.adjust_for_ambient_noise(source2, duration=1)
                SpeakText("Speak after this tone")
                winsound.Beep(frequency, duration)
            #listens for the user's input 
                audio2 = r.listen(source2)
                SpeakText("OK")
                print("Time over, thank you")
            # Using ggogle to recognize audio
                MyText = r.recognize_google(audio2)
                MyText = MyText.lower()
                print("Did you say "+MyText)
                return MyText
                break
              
        except sr.RequestError as e:
            print("Could not request results; {0}".format(e))
          
        except sr.UnknownValueError:
            print("unknown error occured")
 
# Function to listen recorded audio
def listenaudiofile(filename):
# open the file
    with sr.AudioFile(filename) as source:
        # listen for the data (load audio to memory)
        audio_data = r.record(source)
        # recognize (convert from speech to text)
        text = r.recognize_google(audio_data)
        print(text)
        return text
    