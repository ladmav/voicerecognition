#Author: Thiruneelakandan Rajagopal;Rohini Srinivasan
#Method Usage: Common methods for handling the property file
from jproperties import Properties
from wrapper.listenSpeak import *


def get_property_dict():
    configs = Properties()
    configs_dict = {}
    with open('Config.properties', 'rb') as read_prop:
        configs.load(read_prop)
      
    items_view = configs.items()
    print(items_view)
   
    for item in items_view:
        configs_dict[item[0]] = item[1].data
    
    return configs_dict

def append_property(filename,value):
    file1 = open("config.properties","a")#append mode
    file1.writelines(value)
    file1.close()